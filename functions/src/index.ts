import * as functions from 'firebase-functions';
import * as admin from "firebase-admin";
const request = require("request");
admin.initializeApp();


interface Status {
    statusCode: number,
   // body: string,
    timeToResponse: number,
    timestamp: Date,
    error: any
}


// const url = "https://www.colmena.cl/services/rest/webAfiliadosRest/health"
//
//
// function getResponseColmena() {
//     const options = {
//         url: url,
//         json: true,
//     };
//
//     return new Promise(function (resolve, reject) {
//         request(options, function (err: any, resp: any) {
//             if (err) {
//                 return reject({err: err});
//             }
//             return resolve({res: resp});
//         });
//     });
// }
//
//
// exports.scheduledFunction = functions.pubsub.schedule('every 1 minutes').onRun(async () => {
//
//     const start = Date.now();
//     const event = new Date();
//     const options = {
//         weekday: 'long',
//         year: 'numeric',
//         month: 'long',
//         day: 'numeric',
//         hour: 'numeric',
//         minute: 'numeric',
//         second: 'numeric',
//     };
//     functions.logger.info("status colmena", {structureData: true});
//
//     getResponseColmena().then(async (resp: any) => {
//         const status: Status = {
//             statusCode: resp.res.statusCode,
//             body: resp.res.body,
//             timeToResponse: (Date.now() - start),
//             timestamp: event.toLocaleDateString('es-CL', options),
//             error: null,
//         }
//         await admin.firestore().collection('status').doc(status.timestamp.toString()).set(status);
//
//     }).catch(async (err) => {
//         const status: Status = {
//             statusCode: err.statusCode,
//             body: err.body,
//             timeToResponse: (Date.now() - start),
//             timestamp: event.toLocaleDateString('es-CL', options),
//             error: err,
//         }
//         await admin.firestore().collection('status').doc(status.timestamp.toString()).set(status);
//
//     });
//
// });
//
// exports.getStatusColmena = functions.https.onRequest(async (req, res) => {
//     const start = Date.now();
//     const event = new Date();
//     const options = {
//         weekday: 'long',
//         year: 'numeric',
//         month: 'long',
//         day: 'numeric',
//         hour: 'numeric',
//         minute: 'numeric',
//         second: 'numeric',
//     };
//     functions.logger.info("status colmena", {structureData: true});
//
//     getResponseColmena().then(async (resp: any) => {
//         const status: Status = {
//             statusCode: resp.res.statusCode,
//             body: resp.res.body,
//             timeToResponse: (Date.now() - start),
//             timestamp: event.toLocaleDateString('es-CL', options),
//             error: null,
//         }
//
//         await admin.firestore().collection('status').doc(status.timestamp.toString()).set(status).then(() =>
//             res.json(status)
//         );
//         // res.json(status)
//
//     }).catch(async (err) => {
//         const status: Status = {
//             statusCode: err.statusCode,
//             body: err.body,
//             timeToResponse: (Date.now() - start),
//             timestamp: event.toLocaleDateString('es-CL', options),
//             error: err,
//         }
//
//         await admin.firestore().collection('status').doc(status.timestamp.toString()).set(status).then(() =>
//             res.json(status)
//         );
//         //  res.json(status)
//
//     });
//
//
// })
//


const urlDitto = "https://pokeapi.co/api/v2/pokemon/ditto"


function getResponseDitto() {
    const options = {
        url: urlDitto,
        json: true,
    };

    return new Promise(function (resolve, reject) {
        request(options, function (err: any, resp: any) {
            if (err) {
                return reject({err: err});
            }
            return resolve({res: resp});
        });
    });
}


exports.scheduledFunctionDitto = functions.pubsub.schedule('every 5 minutes').onRun(async () => {

    const start = Date.now();
    const event = new Date();

    functions.logger.info("status ditto", {structureData: true});

    getResponseDitto().then(async (resp: any) => {
        const status: Status = {
            statusCode: resp.res.statusCode,
         //   body: resp.res.body,
            timeToResponse: (Date.now() - start),
            timestamp: event,
            error: null,
        }
        await admin.firestore().collection('ditto').doc(status.timestamp.toString()).set(status);

    }).catch(async (err) => {
        const status: Status = {
            statusCode: err.statusCode,
         //   body: err.body,
            timeToResponse: (Date.now() - start),
            timestamp: event,
            error: err,
        }
        await admin.firestore().collection('ditto').doc(status.timestamp.toString()).set(status);

    });

});

exports.getStatusDitto = functions.https.onRequest(async (req, res) => {
    const start = Date.now();
    const event = new Date();

    functions.logger.info("status pokemon", {structureData: true});

    getResponseDitto().then(async (resp: any) => {
        const status: Status = {
            statusCode: resp.res.statusCode,
         //   body: resp.res.body,
            timeToResponse: (Date.now() - start),
            timestamp: event,
            error: null,
        }

        await admin.firestore().collection('ditto').doc(status.timestamp.toString()).set(status).then(() =>
            res.json(status)
        );
        // res.json(status)

    }).catch(async (err) => {
        const status: Status = {
            statusCode: err.statusCode,
         //   body: err.body,
            timeToResponse: (Date.now() - start),
            timestamp: event,
            error: err,
        }

        await admin.firestore().collection('ditto').doc(status.timestamp.toString()).set(status).then(() =>
            res.json(status)
        );
        //  res.json(status)

    });


})








const urlPokemon = "https://pokeapi.co/api/v3/pokemon"


function getResponsePokemon() {
    const options = {
        url: urlPokemon,
        json: true,
    };

    return new Promise(function (resolve, reject) {
        request(options, function (err: any, resp: any) {
            if (err) {
                return reject({err: err});
            }
            return resolve({res: resp});
        });
    });
}


exports.scheduledFunctionPokemon = functions.pubsub.schedule('every 5 minutes').onRun(async () => {

    const start = Date.now();
    const event = new Date();

    functions.logger.info("status colmena", {structureData: true});

    getResponsePokemon().then(async (resp: any) => {
        const status: Status = {
            statusCode: resp.res.statusCode,
          //  body: resp.res.body,
            timeToResponse: (Date.now() - start),
            timestamp: event,
            error: null,
        }
        await admin.firestore().collection('pokemon').doc(status.timestamp.toString()).set(status);

    }).catch(async (err) => {
        const status: Status = {
            statusCode: err.statusCode,
         //   body: err.body,
            timeToResponse: (Date.now() - start),
            timestamp: event,
            error: err,
        }
        await admin.firestore().collection('pokemon').doc(status.timestamp.toString()).set(status);

    });

});

exports.getStatusPokemon = functions.https.onRequest(async (req, res) => {
    const start = Date.now();
    const event = new Date();

    functions.logger.info("status colmena", {structureData: true});

    getResponsePokemon().then(async (resp: any) => {
        const status: Status = {
            statusCode: resp.res.statusCode,
         //   body: resp.res.body,
            timeToResponse: (Date.now() - start),
            timestamp: event,
            error: null,
        }

        await admin.firestore().collection('pokemon').doc(status.timestamp.toString()).set(status).then(() =>
            res.json(status)
        );
        // res.json(status)

    }).catch(async (err) => {
        const status: Status = {
            statusCode: err.statusCode,
          //  body: err.body,
            timeToResponse: (Date.now() - start),
            timestamp: event,
            error: err,
        }

        await admin.firestore().collection('pokemon').doc(status.timestamp.toString()).set(status).then(() =>
            res.json(status)
        );
        //  res.json(status)

    });


})






